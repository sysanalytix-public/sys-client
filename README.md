# Sysanalytix client

Send data to sysanalytix client securely using OIDC and OAuth.

```java

    SysTenantAwareClient client = SysanalytixClientBuilder.tenantAwareClient()
      .withUrl("https://intake-api.sysanalytix.net")
      .withApiKey("41341749-960d-495b-9e3e-e730194e24e8")
      .withName("InternalClient")
      .withTenantId("558e1c34-4108-476b-bd2d-76735deccaf1")
      .withRole("api_sysanalytix_net_data_write")
      .withUsername(System.getenv("SYS_USERNAME"))
      .withPassword(System.getenv("SYS_PASSWORD"))
      .withLoggingLevel(HttpLoggingInterceptor.Level.BODY)
      .withEventTransformer(Transformer.fieldCleanser())
      .build();

    SysResponse response = client.send(Context.builder().withDryRun(true).build(), ApplicationEvent
      .builder()
      .withApplication("List-Order-Component")
      .withEventID("9c59e0e7-34c0-4759-87e9-fb8c4ee9adf3") //Use Time UUID instead of UUID
      .withStatus("SUCCESS")
      .withBundle("Customer-Portal")
      .withEventID("9c59e0e7-34c0-4759-87e9-fb8c4ee9adf3")
      .withHost("portal-server-2")
      .withElementID(UUID.randomUUID().toString())
      .withStatusTimestamp(Instant.now().toEpochMilli())
      .withAddAdditionalInfoItem("typeOfData", "sample")
      .build());

    Assert.assertEquals(response.getCode(), 202);
```

## Bulk Request


```java

    SysTenantAwareClient client = SysanalytixClientBuilder.tenantAwareClient()
      .withUrl("https://intake-api.sysanalytix.net")
      .withApiKey("41341749-960d-495b-9e3e-e730194e24e8")
      .withName("InternalClient")
      .withTenantId("558e1c34-4108-476b-bd2d-76735deccaf1")
      .withRole("api_sysanalytix_net_data_write")
      .withUsername(System.getenv("SYS_USERNAME"))
      .withPassword(System.getenv("SYS_PASSWORD"))
      .withLoggingLevel(HttpLoggingInterceptor.Level.BODY)
      .withEventTransformer(Transformer.fieldCleanser())
      .build();

    SysResponse response = client.sendBulk(Context.builder().withDryRun(true).build(), buildBulkEvents(100));

    Assert.assertEquals(response.getCode(), 202);

```
# Build project 

1. Install maven and java 8
2. run `mvn clean install`
