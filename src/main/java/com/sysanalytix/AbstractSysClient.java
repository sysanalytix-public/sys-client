package com.sysanalytix;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

@Getter
@Setter
public class AbstractSysClient {

  protected List<Predicate<ApplicationEvent>> filters;

  protected List<Function<ApplicationEvent, ApplicationEvent>> transformers;

  protected SysResponse processSingleEvent(final ApplicationEvent event) {
    boolean filtered = filters.stream().filter(f -> f.test(event)).findFirst().isPresent();
    if (filtered) {
      return SysResponse.builder().withHasFiltered(true).build();
    }

    transformers.stream().forEach(t -> {
      t.apply(event);
    });

    return null;
  }

  protected EventHolder processBatchEvents(final ApplicationEventList events) {

    List<ApplicationEvent> inputEvents = events.getEvents();

    List<ApplicationEvent> filteredEvents = new ArrayList<>();
    List<ApplicationEvent> validEvents = new ArrayList<>();

    for (ApplicationEvent event : inputEvents) {
      boolean filtered = filters.stream().filter(f -> f.test(event)).findFirst().isPresent();
      if (filtered) {
        filteredEvents.add(event);
      } else {
        validEvents.add(event);
      }
    }

    validEvents.stream().forEach(event -> {
      transformers.stream().forEach(t -> t.apply(event));
    });

    return EventHolder.builder()
      .withFilteredEvents(filteredEvents)
      .withValidEvents(validEvents)
      .withFinalList(ApplicationEventList.builder().withEvents(validEvents).build())
      .build();
  }

  @Getter
  @Setter
  @Builder(setterPrefix = "with")
  public static class EventHolder {
    private final List<ApplicationEvent> filteredEvents;
    private final List<ApplicationEvent> validEvents;
    private final ApplicationEventList finalList;
  }
}
