package com.sysanalytix;

import lombok.Builder;
import lombok.Data;
import okhttp3.Response;

@Data
@Builder(setterPrefix = "with")
public class SysResponse {
  private boolean hasFiltered;
  private int code;
  private Response response;
}
