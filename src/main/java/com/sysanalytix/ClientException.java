package com.sysanalytix;

public class ClientException extends RuntimeException {
  public ClientException(String message, Throwable e){
    super(message, e);
  }
  public ClientException(String message){
    super(message);
  }
}
