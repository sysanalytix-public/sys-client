package com.sysanalytix;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.NonNull;
import okhttp3.*;

import java.io.IOException;


@Builder
public class SysOAuthClient extends AbstractSysClient implements SysClient {

  @NonNull
  private OkHttpClient client;

  @NonNull
  private String url;

  @NonNull
  private String apiKey;

  @NonNull
  private ObjectMapper objectMapper;

  @NonNull
  private TokenHolder tokenHolder;

  private static final MediaType JSON_TYPE = MediaType.parse("application/json");

  public SysResponse send(@NonNull Context context, @NonNull ApplicationEvent event) throws ClientException {

    SysResponse sysresponse = processSingleEvent(event);
    if (sysresponse != null) {
      return sysresponse;
    }

    String json_body = null;
    try {
      json_body = objectMapper.writeValueAsString(event);
    } catch (JsonProcessingException e) {
      throw new ClientException("Parsing error", e);
    }
    RequestBody body = RequestBody.create(json_body, JSON_TYPE);
    Request request = new Request.Builder()
      .url(url)
      .addHeader("x-api-key", apiKey)
      .addHeader("Accept", "application/json")
      .post(body)
      .build();

    Response response = null;
    try {
      response = client.newCall(request).execute();
    } catch (IOException e) {
      throw new ClientException("Fail to call events endpoint", e);
    } finally {
      if (response != null) {
        response.close();
      }
    }
    return SysResponse.builder().withCode(response.code()).build();
  }

  @Override
  public SysResponse sendBulk(@NonNull Context context, @NonNull ApplicationEventList eventList) throws ClientException {
    String json_body = null;
    try {
      json_body = objectMapper.writeValueAsString(eventList);
    } catch (JsonProcessingException e) {
      throw new ClientException("Parsing error", e);
    }

    HttpUrl bulkUrl = HttpUrl.parse(url).newBuilder().addQueryParameter("bulk", "").build();

    RequestBody body = RequestBody.create(json_body, JSON_TYPE);

    Request request = new Request.Builder()
      .url(bulkUrl)
      .addHeader("x-api-key", apiKey)
      .addHeader("Accept", "application/json")
      .post(body)
      .build();

    Response response = null;
    try {
      response = client.newCall(request).execute();
    } catch (IOException e) {
      throw new ClientException("Fail to call events endpoint", e);
    } finally {
      if (response != null) {
        response.close();
      }
    }
    return SysResponse.builder().withCode(response.code()).build();
  }
}
