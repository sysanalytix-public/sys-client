package com.sysanalytix;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Transformer {

  private static Function<String, String> cleanseString = (value) -> {
    if (value == null || StringUtils.trimToNull(value) == null) {
      return value;
    }
    return StringUtils.trimToEmpty(StringUtils.replaceChars(value, ',', '_'));
  };


  public static Function<ApplicationEvent, ApplicationEvent> fieldCleanser() {
    return applyFunctionToFields(cleanseString);
  }

  public static Function<ApplicationEvent, ApplicationEvent> additionalInfoCleanser() {
    return applyFunctionToAdditionalInfo(cleanseString);
  }

  public static Function<ApplicationEvent, ApplicationEvent> applyFunctionToFields(Function<String, String> function) {
    return (ApplicationEvent event) -> {
      event.setBundle(function.apply(event.getBundle()));
      event.setElementID(function.apply(event.getElementID()));
      event.setExternalID(function.apply(event.getExternalID()));
      event.setHost(function.apply(event.getHost()));
      event.setLocalID(function.apply(event.getLocalID()));
      event.setStatus(function.apply(event.getStatus()));
      event.setParentElementID(function.apply(event.getParentElementID()));
      event.setEventID(function.apply(event.getEventID()));
      event.setApplication(function.apply(event.getApplication()));
      Map<String, String> newMap = new HashMap<>();
      for (Map.Entry<String, String> entry : event.getAdditionalInfo().entrySet()) {
        newMap.put(function.apply(entry.getKey()), function.apply(entry.getValue()));
      }
      event.setAdditionalInfo(newMap);
      return event;
    };
  }

  public static Function<ApplicationEvent, ApplicationEvent> applyFunctionToAdditionalInfo(Function<String, String> function) {
    return (ApplicationEvent event) -> {
      Map<String, String> additionalInfo = event.getAdditionalInfo();
      Map<String, String> newMap = new HashMap<>();
      for (Map.Entry<String, String> entry : additionalInfo.entrySet()) {
        newMap.put(function.apply(entry.getKey()), function.apply(entry.getValue()));
      }
      event.setAdditionalInfo(newMap);
      return event;
    };
  }
}
