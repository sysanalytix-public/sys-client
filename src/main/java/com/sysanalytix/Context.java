package com.sysanalytix;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(setterPrefix = "with")
public class Context {
  private boolean dryRun;
}
