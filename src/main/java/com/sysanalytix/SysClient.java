package com.sysanalytix;

import lombok.NonNull;

public interface SysClient {

  /**
   * Send a single event
   *
   * @param context to pass context to change.
   * @param event   to be send to Sysanalytix platform
   * @return response from platform
   * @throws ClientException in case call fails
   */
  SysResponse send(@NonNull Context context, @NonNull ApplicationEvent event) throws ClientException;

  /**
   * Send micro batch of events. The maximum batch size should be between
   * 50 and 100 events
   *
   * @param context   to pass context to change.
   * @param eventList to be send to Sysanalytix platform
   * @return response from platform and batchId
   * @throws ClientException in case call fails
   */
  SysResponse sendBulk(@NonNull Context context, @NonNull ApplicationEventList eventList) throws ClientException;

}
