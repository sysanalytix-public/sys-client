package com.sysanalytix;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;


@Setter
@Getter
@Builder(setterPrefix = "with")
public class ApplicationEvent {
  @NonNull
  private String elementID;
  private String eventID;
  private String parentElementID;
  private String localID;
  private String externalID;
  private String status;
  @NonNull
  private String application;
  private String bundle;
  private String host;
  private long statusTimestamp;
  private long eventTimestamp;
  private Map<String, String> additionalInfo;

  public static class ApplicationEventBuilder {
    ApplicationEventBuilder withAddAdditionalInfoItem(@NonNull String key, @NonNull String value) {
      if (this.additionalInfo == null) {
        this.withAdditionalInfo(new HashMap<>());
      }
      this.additionalInfo.put(key, value);
      return this;
    }
  }
}
