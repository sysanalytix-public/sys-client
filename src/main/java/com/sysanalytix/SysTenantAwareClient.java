package com.sysanalytix;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.NonNull;
import okhttp3.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

@Builder
public class SysTenantAwareClient extends AbstractSysClient implements SysClient {
  @NonNull
  private OkHttpClient client;

  @NonNull
  private String url;

  @NonNull
  private String apiKey;

  @NonNull
  private String role;

  @NonNull
  private String tenantId;

  @NonNull
  private String name;

  @NonNull
  private ObjectMapper objectMapper;

  private static final MediaType JSON_TYPE = MediaType.parse("application/json;charset=utf-8");

  @Override
  public SysResponse send(@NonNull Context context, @NonNull final ApplicationEvent event) throws ClientException {
    SysResponse sysresponse = processSingleEvent(event);
    if (sysresponse != null) {
      return sysresponse;
    }

    String json_body = null;
    try {
      json_body = objectMapper.writeValueAsString(event);
    } catch (JsonProcessingException e) {
      throw new ClientException("Parsing error", e);
    }
    RequestBody body = RequestBody.create(json_body, JSON_TYPE);

    HttpUrl httpurl = HttpUrl.parse(url).newBuilder().addPathSegments("input/save").build();

    Request.Builder builder = new Request.Builder()
      .url(httpurl)
      .addHeader("x-api-key", apiKey)
      .addHeader("x-tenant-id", tenantId)
      .addHeader("x-roles", role)
      .addHeader("x-subject-id", name)
      .addHeader("Accept", "application/json")
      .post(body);

    if (context.isDryRun()) {
      builder.addHeader("x-dry-run", "true");
    }
    Request request = builder.build();

    Response response = null;
    try {
      response = client.newCall(request).execute();
    } catch (IOException e) {
      throw new ClientException("Fail to call events endpoint", e);
    } finally {
      if (response != null) {
        response.close();
      }
    }
    return SysResponse.builder().withCode(response.code()).withResponse(response).build();
  }

  @Override
  public SysResponse sendBulk(@NonNull Context context, @NonNull ApplicationEventList events) throws ClientException {

    final EventHolder eventHolder = processBatchEvents(events);

    final ApplicationEventList finalList = eventHolder.getFinalList();

    if (finalList.getEvents().isEmpty()) {
      return SysResponse.builder().withHasFiltered(true).build();
    }

    String b64json = null;
    try {
      b64json = Base64.getEncoder().encodeToString(objectMapper.writeValueAsString(finalList).getBytes(StandardCharsets.UTF_8));
    } catch (JsonProcessingException e) {
      throw new ClientException("Parsing error", e);
    }

    HttpUrl bulkUrl = HttpUrl.parse(url).newBuilder().addPathSegments("input/save").addQueryParameter("bulk", UUID.randomUUID().toString()).build();

    RequestBody body = RequestBody.create(b64json, JSON_TYPE);

    Request.Builder builder = new Request.Builder()
      .url(bulkUrl)
      .addHeader("x-api-key", apiKey)
      .addHeader("x-tenant-id", tenantId)
      .addHeader("x-roles", role)
      .addHeader("x-subject-id", name)
      .addHeader("Content-Type", "application/json;charset=utf-8")
      .addHeader("Content-Encoding", "gzip")
      .post(body);

    if (context.isDryRun()) {
      builder.addHeader("x-dry-run", "true");
    }
    Request request = builder.build();

    Response response = null;
    try {
      response = client.newCall(request).execute();
    } catch (IOException e) {
      throw new ClientException("Fail to call events endpoint", e);
    } finally {
      if (response != null) {
        response.close();
      }
    }

    return SysResponse.builder().withCode(response.code()).withResponse(response).build();
  }
}
