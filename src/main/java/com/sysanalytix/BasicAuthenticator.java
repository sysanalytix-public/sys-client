package com.sysanalytix;

import lombok.Builder;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;


@Builder(setterPrefix = "with")
public class BasicAuthenticator implements Authenticator {

  private final String username;

  private final String password;

  private static int responseCount(Response response) {
    int result = 1;
    while ((response = response.priorResponse()) != null) {
      result++;
    }
    return result;
  }

  @Nullable
  @Override
  public Request authenticate(@Nullable Route route, @NotNull Response response) throws IOException {
    String credential = Credentials.basic(username, password);
    if (responseCount(response) >= 3) {
      return null;
    }
    return response.request().newBuilder().header("Authorization", credential).build();
  }
}
