package com.sysanalytix;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Synchronized;
import okhttp3.*;


@Builder
public class TokenHolder {

  private String access_token;

  private final OkHttpClient client = new OkHttpClient();

  private final ObjectMapper objectMapper = new ObjectMapper();

  private final String scope = "api.sysanalytix.net/data.write";

  private final String granType = "client_credentials";

  @NonNull
  private String tokenUrl;

  @NonNull
  private String clientID;

  @NonNull
  private String clientSecret;

  @Synchronized
  public String refreshToken() {
    this.access_token = refreshAccessToken();
    return this.access_token;
  }

  @Synchronized
  public String getToken() {
    return access_token;
  }

  private String refreshAccessToken() {
    RequestBody formBody = new FormBody.Builder()
      .add("client_id", clientID)
      .add("client_secret", clientSecret)
      .add("scope", scope)
      .add("grant_type", granType)
      .build();

    Request request = new Request.Builder()
      .url(tokenUrl)
      .post(formBody)
      .build();

    Call call = client.newCall(request);
    Response response = null;
    try {
      response = call.execute();
      if (response.isSuccessful()) {
        return objectMapper.readValue(response.body().string(), Token.class).access_token;
      }
    } catch (Exception e) {
      throw new ClientException("Error getting access token", e);
    }finally {
      if (response!= null ){
        response.close();
      }
    }
    return null;
  }

  @Data
  public static class Token {
    private String access_token;
    private int expires_in;
    private String token_type;
  }
}
