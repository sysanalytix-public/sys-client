package com.sysanalytix;

import lombok.Builder;
import lombok.NonNull;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.net.HttpURLConnection;

@Builder
public class AccessTokenInterceptor implements Interceptor {

  @NonNull
  private TokenHolder tokenHolder;

  @Override
  public Response intercept(Chain chain) throws IOException {
    String accessToken = tokenHolder.getToken();
    Request request = newRequestWithAccessToken(chain.request(), accessToken);
    Response response = chain.proceed(request);
    if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
      response.close();
      synchronized (this) {
        String newAccessToken = tokenHolder.refreshToken();
        return chain.proceed(newRequestWithAccessToken(request, newAccessToken));
      }
    }
    return response;
  }

  @NonNull
  private Request newRequestWithAccessToken(@NonNull Request request, String accessToken) {
    return request.newBuilder()
      .header("Authorization", "Bearer " + accessToken)
      .build();
  }
}
