package com.sysanalytix;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;


public class SysanalytixClientBuilder {

  public static DefaultClient.DefaultClientBuilder oauthClient() {
    return new DefaultClient.DefaultClientBuilder();
  }

  public static TenantAwareClient.TenantAwareClientBuilder tenantAwareClient() {
    return new TenantAwareClient.TenantAwareClientBuilder();
  }


  @Builder(setterPrefix = "with")
  @Data
  public static class DefaultClient {
    @NonNull
    private String url;
    @NonNull
    private String tokenUrl;
    @NonNull
    private String apiKey;
    @NonNull
    private String clientId;
    @NonNull
    private String clientSecret;

    private HttpLoggingInterceptor.Level loggingLevel;

    private OkHttpClient client;

    public static class DefaultClientBuilder {
      public SysOAuthClient build() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.level(HttpLoggingInterceptor.Level.NONE);
        if (loggingLevel != null) {
          loggingInterceptor.level(loggingLevel);
        }

        TokenHolder holder = TokenHolder.builder()
          .clientID(this.clientId)
          .clientSecret(this.clientSecret)
          .tokenUrl(tokenUrl).build();

        AccessTokenInterceptor accessTokenInterceptor = AccessTokenInterceptor.builder()
          .tokenHolder(holder)
          .build();

        OkHttpClient.Builder builder = null;
        if (client == null) {
          builder = new OkHttpClient().newBuilder();
        } else {
          builder = client.newBuilder();
        }

        client = builder
          .addInterceptor(accessTokenInterceptor)
          .addInterceptor(new GzipInterceptor())
          .addInterceptor(loggingInterceptor)
          .build();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return SysOAuthClient.builder().tokenHolder(holder).objectMapper(objectMapper).apiKey(this.apiKey).client(client).url(this.url).build();
      }
    }
  }

  @Builder(setterPrefix = "with")
  @Data
  public static class TenantAwareClient {
    @NonNull
    private String url;
    @NonNull
    private String tenantId;
    @NonNull
    private String apiKey;
    @NonNull
    private String role;
    @NonNull
    private String name;
    @NonNull
    private String username;
    @NonNull
    private String password;

    private OkHttpClient client;

    private List<Predicate<ApplicationEvent>> filters;

    private List<Function<ApplicationEvent, ApplicationEvent>> transformers;

    private HttpLoggingInterceptor.Level loggingLevel;

    public static class TenantAwareClientBuilder {

      public TenantAwareClientBuilder withEventFilter(@NonNull Predicate<ApplicationEvent> filter) {
        if (this.filters == null) {
          this.filters = new ArrayList<>();
        }
        this.filters.add(filter);
        return this;
      }

      public TenantAwareClientBuilder withEventTransformer(@NonNull Function<ApplicationEvent, ApplicationEvent> transformer) {
        if (this.transformers == null) {
          this.transformers = new ArrayList<>();
        }
        this.transformers.add(transformer);
        return this;
      }


      public SysTenantAwareClient build() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.level(HttpLoggingInterceptor.Level.NONE);
        if (loggingLevel != null) {
          loggingInterceptor.level(loggingLevel);
        }

        OkHttpClient.Builder builder = null;
        if (client == null) {
          builder = new OkHttpClient().newBuilder();
        } else {
          builder = client.newBuilder();
        }

        client = builder
          .addInterceptor(new GzipInterceptor())
          .addInterceptor(loggingInterceptor)
          .authenticator(BasicAuthenticator.builder().withPassword(password).withUsername(username).build())
          .build();


        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        SysTenantAwareClient sysClient = SysTenantAwareClient.builder()
          .apiKey(apiKey)
          .name(name)
          .client(client)
          .objectMapper(objectMapper)
          .url(url)
          .role(role)
          .tenantId(tenantId).build();

        sysClient.setFilters(this.filters == null ? new ArrayList<>() : this.filters);
        sysClient.setTransformers(this.transformers == null ? new ArrayList<>() : this.transformers);

        return sysClient;
      }
    }
  }
}
