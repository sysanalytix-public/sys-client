package com.sysanalytix;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder(setterPrefix = "with")
public class ApplicationEventList {
  @NonNull
  private List<ApplicationEvent> events;

  public static class ApplicationEventListBuilder {
    ApplicationEventList.ApplicationEventListBuilder withApplicationEventItem(@NonNull ApplicationEvent applicationEvent) {
      if (this.events == null) {
        this.withEvents(new ArrayList<>());
      }
      this.events.add(applicationEvent);
      return this;
    }
  }
}
