package com.sysanalytix;

import okhttp3.logging.HttpLoggingInterceptor;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.util.UUID;
import java.util.stream.IntStream;

public class TenantAwareClientTest {

  @Test
  public void testTenantAwareClient_SingleRequest() {

    SysTenantAwareClient client = SysanalytixClientBuilder.tenantAwareClient()
      .withUrl("https://intake-api.sysanalytix.net")
      .withApiKey("41341749-960d-495b-9e3e-e730194e24e8")
      .withName("InternalClient")
      .withTenantId("558e1c34-4108-476b-bd2d-76735deccaf1")
      .withRole("api_sysanalytix_net_data_write")
      .withUsername(System.getenv("SYS_USERNAME"))
      .withPassword(System.getenv("SYS_PASSWORD"))
      .withLoggingLevel(HttpLoggingInterceptor.Level.BODY)
      .withEventTransformer(Transformer.fieldCleanser())
      .build();

    SysResponse response = client.send(Context.builder().withDryRun(true).build(), ApplicationEvent
      .builder()
      .withApplication("List-Order-Component")
      .withEventID("9c59e0e7-34c0-4759-87e9-fb8c4ee9adf3") //Use Time UUID instead of UUID
      .withStatus("SUCCESS")
      .withBundle("Customer-Portal")
      .withEventID("9c59e0e7-34c0-4759-87e9-fb8c4ee9adf3")
      .withHost("portal-server-2")
      .withElementID(UUID.randomUUID().toString())
      .withStatusTimestamp(Instant.now().toEpochMilli())
      .withAddAdditionalInfoItem("typeOfData", "sample")
      .build());

    Assert.assertEquals(response.getCode(), 202);
  }

  @Test
  public void testTenantAwareClient_BulkRequest() {

    SysTenantAwareClient client = SysanalytixClientBuilder.tenantAwareClient()
      .withUrl("https://intake-api.sysanalytix.net")
      .withApiKey("41341749-960d-495b-9e3e-e730194e24e8")
      .withName("InternalClient")
      .withTenantId("558e1c34-4108-476b-bd2d-76735deccaf1")
      .withRole("api_sysanalytix_net_data_write")
      .withUsername(System.getenv("SYS_USERNAME"))
      .withPassword(System.getenv("SYS_PASSWORD"))
      .withLoggingLevel(HttpLoggingInterceptor.Level.BODY)
      .withEventTransformer(Transformer.fieldCleanser())
      .build();

    SysResponse response = client.sendBulk(Context.builder().withDryRun(true).build(), buildBulkEvents(100));

    Assert.assertEquals(response.getCode(), 202);

  }


  private static ApplicationEventList buildBulkEvents(int num) {
    ApplicationEventList.ApplicationEventListBuilder builder = ApplicationEventList.builder();
    IntStream.range(0, num).forEach((i) -> {
      builder.withApplicationEventItem(
        ApplicationEvent
          .builder()
          .withApplication("List-Order-Component")
          .withStatus("SUCCESS")
          .withBundle("Customer-Portal")
          .withHost("portal-server-2")
          .withEventID(UUID.randomUUID().toString()) //Use Time UUID instead of UUID
          .withElementID(UUID.randomUUID().toString())
          .withStatusTimestamp(Instant.now().toEpochMilli())
          .withAddAdditionalInfoItem("typeOfData", "sample")
          .build());
    });
    return builder.build();
  }
}
