package com.sysanalytix;

import org.junit.Test;

import java.time.Instant;
import java.util.UUID;

public class OAuthClientTest {

  @Test
  public void send_single_event() {
    SysOAuthClient client = SysanalytixClientBuilder.oauthClient()
      .withApiKey(System.getenv("SYS_APIKEY"))
      .withClientId(System.getenv("SYS_CLIENT_ID"))
      .withClientSecret(System.getenv("SYS_SECRET"))
      .withTokenUrl(System.getenv("SYS_TOKEN_URL"))
      .withUrl(System.getenv("SYS_ENDPOINT_URL")).build();


    SysResponse response = client.send(Context.builder().build(), ApplicationEvent
      .builder()
      .withApplication("List-Order-Component")
      .withStatus("SUCCESS")
      .withBundle("Customer-Portal")
      .withHost("portal-server-2")
      .withElementID(UUID.randomUUID().toString())
      .withEventID("9c59e0e7-34c0-4759-87e9-fb8c4ee9adf3") //Use Time UUID instead of UUID
      .withStatusTimestamp(Instant.now().toEpochMilli())
      .withAddAdditionalInfoItem("typeOfData", "sample")
      .build());

    System.out.println(response.getCode());
  }
}
