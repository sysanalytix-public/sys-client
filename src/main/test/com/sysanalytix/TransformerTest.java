package com.sysanalytix;

import okhttp3.logging.HttpLoggingInterceptor;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class TransformerTest {

  @Test
  public void clean_data() {

    ApplicationEvent bad_event = ApplicationEvent
      .builder()
      .withApplication(" List-Or,der-Component ")
      .withStatus(" SUC,CESS" )
      .withBundle(" Customer-Portal ")
      .withHost(" portal-s,erver-2 ")
      .withEventID(UUID.randomUUID().toString()) //Use Time UUID instead of UUID
      .withElementID(UUID.randomUUID().toString())
      .withStatusTimestamp(Instant.now().toEpochMilli())
      .withAddAdditionalInfoItem(" space ", " sam,ple ")
      .withAddAdditionalInfoItem(" com,ma ", " sa,mple ")
      .build();

    ApplicationEvent cleanse_event = Transformer.fieldCleanser().apply(bad_event);
    Assert.assertEquals(cleanse_event.getApplication(), "List-Or_der-Component");
    Assert.assertEquals(cleanse_event.getStatus(), "SUC_CESS");
    Assert.assertEquals(cleanse_event.getBundle(), "Customer-Portal");
    Assert.assertEquals(cleanse_event.getHost(), "portal-s_erver-2");
    Map<String, String> additionalInfo = cleanse_event.getAdditionalInfo();
    List<String> sortedKeys = additionalInfo.keySet().stream().sorted().collect(Collectors.toList());
    List<String> sortedValues = additionalInfo.values().stream().sorted().collect(Collectors.toList());
    Assert.assertThat(sortedKeys, CoreMatchers.hasItems("space","com_ma"));
    Assert.assertThat(sortedValues, CoreMatchers.hasItems("sam_ple","sa_mple"));
  }
}
